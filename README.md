# SporTable

Na srácok egykis használati utasítás azokhoz a dolgokhoz amit csináltam.


Legelőször is mindenki szendje le a gépére a maven-t .


Be állítottam hogy a backend szerverünknek legyen swagger-e. Ez annyit tesz hogy szép formában láthatjuk a 
végpontokat, és nem nekünk kézzel kell szórakozni azzal hogy össze állítsunk egy json obj pl a creat függvényeknél.
Illetve egyszerüen csak átláthatóbb.
Ezt elérni ezen a linken tudjátok : http://localhost:8080/swagger-ui.html#/

Ahoz hogy ezen az oldoalon megjelenjenek a végpontok pár meg kell csinálni ezek a következők: 

1,
	ha új controller-t csináltok rakjátok az osztályra ezt :@Api(value = "/court", produces = "application/json")
	ez meg csinálja nektek a felületen a lenyiló menüt. mint ahogy a coutrt-nál van és
	ha rá katintottok akkor alatta lesznek a függvények.
2,
	minden függvény fölöé kerüljön egy ilyen :@ApiOperation(value="Egy pálya id alpaján történő lekérdezése",response=Court.class)
	value anyit tesz hogy e írás a függvényről az oldalon lehet látni majd,illetve mellete hogy mi fog vissza menni a végponton.
	
Ennyi volt.
Valamint srácok szerintem használjon @DeleteMapping,@PostMapping ahol ilyen http hivás van.

Került be egy ugy nevezett mapper. Ez annyit csinál hogy helyettünk le generálja a másolást végző függvényeket.
Pl : valami.set(ez.get()) ilyenek.

Ezeket ugy tudjátok használni,hogy:
1 - megfelelő táblához megfelelő mapper osztályt kell létre hozni ,annak amintájára amit csináltam.
2 - toEntity illetve toDTO szoktak lenni a konvencionális elnevzések ahogy tudom. Ezek ugye entity-t csinálsz dto-t csinálsz
	(Listát is tud meppelni)
Egyszerüen be rántottok az adot service-be egy ilyen mapper példányt, hivatkoztok a függvényére és kész
Átmeppelődik egyik obj-t a másikra.
Használatához mindig le kell generáltatni ezeket .
Ezt ugy tudjátok hogy projectk könyvtár consolban -> mvn clean package 
Majd projeckt ,eclipsen belül.
jobclik a projectre -> maven -> update project
build path -> configure build path ->add forlder -> target -> generated-source -> annota... mellé egy pipa a négyzetbe
és kész is.
Ezt a mvn clean dolgot mindig meg kell csinálni ha új mapper-t vagy benne függvényt hoztatok létre. a hozáadást elég egyszer mm a folderes részt.





