package com.projekteszkozok.SporTable_backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projekteszkozok.SporTable_backend.model.User;
import com.projekteszkozok.SporTable_backend.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
@Api(value="/user",produces ="application/json")
@RequestMapping("/user")
@RestController
public class UserController {
	
	@Autowired
	private UserService service;
	@ApiOperation(value="Egy felhasználó id alpaján történő lekérdezése",response=User.class)
	@GetMapping("/{id}")
	public ResponseEntity<User> getUser(@PathVariable Long id) {
		return new ResponseEntity<User>(service.getUserById(id),HttpStatus.OK);
	}
}
