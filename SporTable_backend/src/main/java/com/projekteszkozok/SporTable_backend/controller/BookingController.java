package com.projekteszkozok.SporTable_backend.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projekteszkozok.SporTable_backend.DTOs.BookingInputDTO;
import com.projekteszkozok.SporTable_backend.model.Booking;
import com.projekteszkozok.SporTable_backend.service.BookingService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/booking", produces = "application/json")
@RequestMapping("/booking")
@RestController
public class BookingController {
	
	@Autowired
	private BookingService bookingService;
	
	@ApiOperation(value="Egy foglalás id alpaján történő lekérdezése",response=Booking.class)
	@GetMapping("/{id}")
	public ResponseEntity<Booking> getBooking(@PathVariable Long id) {
		Optional<Booking> booking = bookingService.getBooking(id);
		
		if(booking.isPresent()) {
			return new ResponseEntity<Booking>(booking.get(),HttpStatus.OK);
		}
		return new ResponseEntity<Booking>(new Booking(),HttpStatus.NOT_FOUND);
		
	}
	@ApiOperation(value="Foglalások listája",response=Booking.class)
	@GetMapping("")
	public ResponseEntity<List<Booking>> getBookings() {
		return new ResponseEntity<List<Booking>>(bookingService.getBookings(),HttpStatus.OK);
	}
	
	@ApiOperation(value="Foglalás törlése",response=Booking.class)
	@DeleteMapping("/{id}")
	public void deleteBookings(@PathVariable Long id) {
		bookingService.deleteBooking(id);
	}
	@ApiOperation(value="Foglalás létrehozása",response=Booking.class)
	@PostMapping("")
	public Booking createBookings(BookingInputDTO booking) {
		return bookingService.createBooking(booking);
	}
}
