package com.projekteszkozok.SporTable_backend.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projekteszkozok.SporTable_backend.DTOs.CourtInputDTO;
import com.projekteszkozok.SporTable_backend.model.Court;
import com.projekteszkozok.SporTable_backend.model.User;
import com.projekteszkozok.SporTable_backend.service.CourtService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/court", produces = "application/json")
@RequestMapping("/court")
@RestController
public class CourtController {
	@Autowired
	private CourtService courtService;
	
	@ApiOperation(value="Egy pálya id alpaján történő lekérdezése",response=Court.class)
	@GetMapping("/{id}")
	public ResponseEntity<Court> getCourt(@PathVariable Long id) {
		Optional<Court> court = courtService.getCourt(id);
		
		if(court.isPresent()) {
			return new ResponseEntity<Court>(court.get(),HttpStatus.OK);
		}
		return new ResponseEntity<Court>(new Court(),HttpStatus.NOT_FOUND);
		
	}
	@ApiOperation(value="Pályák listája",response=Court.class)
	@GetMapping("")
	public ResponseEntity<List<Court>> getCourts() {
		return new ResponseEntity<List<Court>>(courtService.getCourts(),HttpStatus.OK);
	}
	
	@ApiOperation(value="Pálya törlése",response=Court.class)
	@DeleteMapping("/{id}")
	public void deleteCourts(@PathVariable Long id) {
		courtService.deleteCourt(id);
	}
	@ApiOperation(value="Pálya létrehozása",response=Court.class)
	@PostMapping("")
	public Court createCourts(CourtInputDTO court) {
		return courtService.createCourt(court);
	}
}
