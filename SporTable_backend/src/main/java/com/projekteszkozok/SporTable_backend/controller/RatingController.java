package com.projekteszkozok.SporTable_backend.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projekteszkozok.SporTable_backend.DTOs.RatingInputDTO;
import com.projekteszkozok.SporTable_backend.model.Rating;
import com.projekteszkozok.SporTable_backend.service.RatingService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/rating", produces = "application/json")
@RequestMapping("/rating")
@RestController
public class RatingController {
	@Autowired
	private RatingService ratingService;
	
	@ApiOperation(value="Egy értékelés id alpaján történő lekérdezése",response=Rating.class)
	@GetMapping("/{id}")
	public ResponseEntity<Rating> getRating(@PathVariable Long id) {
		Optional<Rating> rating = ratingService.getRating(id);
		
		if(rating.isPresent()) {
			return new ResponseEntity<Rating>(rating.get(),HttpStatus.OK);
		}
		return new ResponseEntity<Rating>(new Rating(),HttpStatus.NOT_FOUND);
		
	}
	
	@ApiOperation(value="Értékelések listája",response=Rating.class)
	@GetMapping("")
	public ResponseEntity<List<Rating>> getRatings() {
		return new ResponseEntity<List<Rating>>(ratingService.getRatings(),HttpStatus.OK);
	}
	
	@ApiOperation(value="Értékelés törlése",response=Rating.class)
	@DeleteMapping("/{id}")
	public void deleteRatings(@PathVariable Long id) {
		ratingService.deleteRating(id);
	}
	
	@ApiOperation(value="Értékelés létrehozása",response=Rating.class)
	@PostMapping("")
	public Rating createRatings(RatingInputDTO rating) {
		return ratingService.createRating(rating);
	}

}
