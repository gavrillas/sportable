package com.projekteszkozok.SporTable_backend.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.projekteszkozok.SporTable_backend.DTOs.CourtInputDTO;
import com.projekteszkozok.SporTable_backend.model.Court;

@Mapper(componentModel = "spring")
public interface CourtMapper {
	
	
	Court toEntity(CourtInputDTO courtInputDTO);
}
