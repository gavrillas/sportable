package com.projekteszkozok.SporTable_backend.mappers;

import org.mapstruct.Mapper;

import com.projekteszkozok.SporTable_backend.DTOs.BookingInputDTO;
import com.projekteszkozok.SporTable_backend.model.Booking;

@Mapper(componentModel = "spring")
public interface BookingMapper {

	Booking toEntity(BookingInputDTO bookingInputDTO);
}
