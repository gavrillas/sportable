package com.projekteszkozok.SporTable_backend.mappers;

import org.mapstruct.Mapper;

import com.projekteszkozok.SporTable_backend.DTOs.RatingInputDTO;
import com.projekteszkozok.SporTable_backend.model.Rating;

@Mapper(componentModel = "spring")
public interface RatingMapper {
	
	Rating toEntity(RatingInputDTO ratingtInputDTO);

}
