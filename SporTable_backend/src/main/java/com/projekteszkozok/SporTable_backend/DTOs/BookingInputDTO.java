package com.projekteszkozok.SporTable_backend.DTOs;



import lombok.Data;

@Data
public class BookingInputDTO {
	
	private Long courtId;
	private Long userId;
	private String bookingTime;
	private String bookingStatus;

}
