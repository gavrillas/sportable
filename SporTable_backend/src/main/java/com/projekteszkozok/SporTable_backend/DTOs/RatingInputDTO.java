package com.projekteszkozok.SporTable_backend.DTOs;

import com.projekteszkozok.SporTable_backend.model.Rating.Role;

import lombok.Data;

@Data
public class RatingInputDTO {	
	
	private Long userId;
	private Long courtId;
	private float ratingPoint;
	private String ratingText;
	private Role role;
	private boolean isdel;
}
