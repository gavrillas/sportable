package com.projekteszkozok.SporTable_backend.DTOs;

import com.projekteszkozok.SporTable_backend.model.Court.Status;

import lombok.Data;
@Data
public class CourtInputDTO {
	private String name;
	private String sport;
	private String image;
	private Status status;
	private String address;
	private Integer price;
	private String description;
}
