package com.projekteszkozok.SporTable_backend.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projekteszkozok.SporTable_backend.model.Booking;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {
	
}
