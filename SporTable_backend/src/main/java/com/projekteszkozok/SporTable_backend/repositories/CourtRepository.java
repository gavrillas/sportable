package com.projekteszkozok.SporTable_backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projekteszkozok.SporTable_backend.model.Court;

public interface CourtRepository extends JpaRepository<Court, Long> {

}
