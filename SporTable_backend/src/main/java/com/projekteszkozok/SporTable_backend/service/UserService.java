package com.projekteszkozok.SporTable_backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projekteszkozok.SporTable_backend.model.User;
import com.projekteszkozok.SporTable_backend.repositories.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository repo;
	
	public User getUserById(Long id) {
		return repo.findById(id).get();
	}
}
