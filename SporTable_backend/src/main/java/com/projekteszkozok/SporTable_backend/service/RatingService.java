package com.projekteszkozok.SporTable_backend.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projekteszkozok.SporTable_backend.DTOs.RatingInputDTO;
import com.projekteszkozok.SporTable_backend.mappers.RatingMapper;
import com.projekteszkozok.SporTable_backend.model.Rating;
import com.projekteszkozok.SporTable_backend.repositories.RatingRepository;

@Service
public class RatingService {

	@Autowired
	private RatingRepository ratingRepository;
	
	@Autowired
	private RatingMapper ratingMapper;
	
	public List<Rating> getRatings(){
		return this.ratingRepository.findAll();
	}
	
	public Optional<Rating> getRating(Long id) {
		return this.ratingRepository.findById(id);
	}
	
	public void deleteRating(Long id) {
		this.ratingRepository.deleteById(id);
	}
	
	public Rating createRating(RatingInputDTO rating) {
		return this .ratingRepository.saveAndFlush(ratingMapper.toEntity(rating));
	}
}
