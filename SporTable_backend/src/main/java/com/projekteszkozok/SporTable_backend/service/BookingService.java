package com.projekteszkozok.SporTable_backend.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projekteszkozok.SporTable_backend.DTOs.BookingInputDTO;
import com.projekteszkozok.SporTable_backend.mappers.BookingMapper;
import com.projekteszkozok.SporTable_backend.model.Booking;
import com.projekteszkozok.SporTable_backend.repositories.BookingRepository;

@Service
public class BookingService {

	@Autowired
	private BookingRepository bookingRepository;
	@Autowired
	private BookingMapper bookingMapper;
	
	
	public List<Booking> getBookings(){
		return this.bookingRepository.findAll();
	}
	
	public Optional<Booking> getBooking(Long id) {
		return this.bookingRepository.findById(id);
	}
	
	public void deleteBooking(Long id) {
		this.bookingRepository.deleteById(id);
	}
	public Booking createBooking(BookingInputDTO booking) {
		return this.bookingRepository.saveAndFlush(bookingMapper.toEntity(booking));
	}
}
