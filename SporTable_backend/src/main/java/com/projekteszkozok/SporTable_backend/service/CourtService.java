package com.projekteszkozok.SporTable_backend.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projekteszkozok.SporTable_backend.DTOs.CourtInputDTO;
import com.projekteszkozok.SporTable_backend.mappers.CourtMapper;
import com.projekteszkozok.SporTable_backend.model.Court;
import com.projekteszkozok.SporTable_backend.repositories.CourtRepository;

@Service
public class CourtService {
	@Autowired
	private CourtRepository courtRepository;
	@Autowired
	private CourtMapper courtMapper;
	
	public List<Court> getCourts(){
		return this.courtRepository.findAll();
	}
	
	public Optional<Court> getCourt(Long id) {
		return this.courtRepository.findById(id);
	}
	
	public void deleteCourt(Long id) {
		this.courtRepository.deleteById(id);
	}
	public Court createCourt(CourtInputDTO court) {
		return this.courtRepository.saveAndFlush(courtMapper.toEntity(court));
	}
}
