package com.projekteszkozok.SporTable_backend.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import lombok.Data;

@Entity
@Data
public class Rating {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Long userId;
	
	private Long courtId;
	
	private float ratingPoint;
	
	private String ratingText;
	
	@Enumerated(EnumType.STRING)
	private Role role;
	
	private boolean isdel;
	
	public enum Role {
    	COURT, USER,
    }

}
