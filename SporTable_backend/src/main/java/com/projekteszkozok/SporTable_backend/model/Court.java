package com.projekteszkozok.SporTable_backend.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Court {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String sport;
	private String image;
	@Enumerated(EnumType.STRING)
	private Status status;
	private String address;
	private Integer price;
	private String description;
	public enum Status{
		OPEN,CLOSED;
	}
}
