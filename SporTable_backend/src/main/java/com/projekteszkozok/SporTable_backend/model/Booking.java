package com.projekteszkozok.SporTable_backend.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Booking {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Long courtId;
	
	private Long userId;
	
	private String bookingTime;
	
	private String bookingStatus; //ez talán enum legyen
}
