package com.projekteszkozok.SporTable_backend.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class User {

	@Id
	private Long id;
	
	private String username;
	
	private String email;
	
	private String password;
	
	private String tel;
	
	@Enumerated(EnumType.STRING)
	private Role role;
	
	private String birthDate;
	
	private boolean isdel;
	
	//private List<Court> Courts;
	
	public enum Role {
    	ADMIN, LENDER, BORROWER
    }
}
